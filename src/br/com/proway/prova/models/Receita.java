package br.com.proway.prova.models;

import java.util.ArrayList;

public class Receita {
    private Long id;
    private String medicoResponsavel;
    private String cliente;
    private ArrayList<Medicamento> medicametos;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMedicoResponsavel() {
        return medicoResponsavel;
    }

    public void setMedicoResponsavel(String medicoResponsavel) {
        this.medicoResponsavel = medicoResponsavel;
    }

    public String getCliente() {
        return cliente;
    }

    public void setCliente(String cliente) {
        this.cliente = cliente;
    }

    public ArrayList<Medicamento> getMedicametos() {
        return medicametos;
    }

    public void setMedicametos(ArrayList<Medicamento> medicametos) {
        this.medicametos = medicametos;
    }
}
