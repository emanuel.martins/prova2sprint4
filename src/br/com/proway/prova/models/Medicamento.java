package br.com.proway.prova.models;

import java.util.ArrayList;
import java.util.Date;

public class Medicamento {
    private Long id;
    private String nome;
    private String descricao;
    private Double preco;
    private Date validade;
    private ArrayList<Venda> vendasMedicamento;
    private ArrayList<Receita> receitasMedicamento;

    public Medicamento() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Double getPreco() {
        return preco;
    }

    public void setPreco(Double preco) {
        this.preco = preco;
    }

    public Date getValidade() {
        return validade;
    }

    public void setValidade(Date validade) {
        this.validade = validade;
    }

    public ArrayList<Venda> getVendasMedicamento() {
        return vendasMedicamento;
    }

    public void setVendasMedicamento(ArrayList<Venda> vendasMedicamento) {
        this.vendasMedicamento = vendasMedicamento;
    }

    public ArrayList<Receita> getReceitasMedicamento() {
        return receitasMedicamento;
    }

    public void setReceitasMedicamento(ArrayList<Receita> receitasMedicamento) {
        this.receitasMedicamento = receitasMedicamento;
    }
}
