package br.com.proway.prova.models;

import java.util.ArrayList;

public class Estoque {
    private Long id;
    private String numeroEstoque;
    private ArrayList<Medicamento> medicamentos;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNumeroEstoque() {
        return numeroEstoque;
    }

    public void setNumeroEstoque(String numeroEstoque) {
        this.numeroEstoque = numeroEstoque;
    }

    public ArrayList<Medicamento> getMedicamentos() {
        return medicamentos;
    }

    public void setMedicamentos(ArrayList<Medicamento> medicamentos) {
        this.medicamentos = medicamentos;
    }
}
