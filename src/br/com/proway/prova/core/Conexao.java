package br.com.proway.prova.core;

import javax.swing.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Conexao {
    public static Connection conexao;
    private String driver = "org.postgresql.Driver";
    private String caminho = "jdbc:postgresql://localhost:5432/farmacia";
    private String usuario = "postgres";
    private String senha = "0000";

    public void conectar(){
        try {
            System.setProperty("jdbc.Drivers", driver);
            conexao = DriverManager.getConnection(caminho, usuario, senha);
            JOptionPane.showMessageDialog(null, "Conectado com sucesso!"
                    , "Banco de Dados", JOptionPane.INFORMATION_MESSAGE);
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro de conexão!\nERRO: "
                    + ex.getMessage(), "Banco de Dados", JOptionPane.INFORMATION_MESSAGE);
        }
    }

    public void fecharConexao() {
        try {
            conexao.close();
            JOptionPane.showMessageDialog(null
                    , "Conexão fechada com sucesso!", "Banco de Dados"
                    , JOptionPane.INFORMATION_MESSAGE);
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Erro ao fechar a conexão!\nERRO: "
                    + e.getMessage(), "Banco de Dados", JOptionPane.INFORMATION_MESSAGE);
        }
    }
}
