package br.com.proway.prova.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Translator {
    public Date toDate(String sentence) {
        try {
            return new SimpleDateFormat("dd-MM-yyyy").parse(sentence);
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
    }
}
