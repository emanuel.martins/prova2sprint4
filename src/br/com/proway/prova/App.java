package br.com.proway.prova;

import br.com.proway.prova.core.Conexao;
import br.com.proway.prova.dao.EstoqueDAO;
import br.com.proway.prova.dao.MedicamentoDAO;
import br.com.proway.prova.dao.ReceitaDAO;
import br.com.proway.prova.models.Estoque;
import br.com.proway.prova.models.Medicamento;
import br.com.proway.prova.models.Receita;
import br.com.proway.prova.models.Venda;
import br.com.proway.prova.utils.Translator;

import javax.swing.*;
import java.sql.Date;
import java.util.ArrayList;

public class App {
    App() {
        Translator translator = new Translator();
        Conexao conexao = new Conexao();
        conexao.conectar();

        EstoqueDAO estoqueDAO = new EstoqueDAO();
        ReceitaDAO receitaDAO = new ReceitaDAO();
        MedicamentoDAO medicamentoDAO = new MedicamentoDAO();

        while (true) {
            String[] opcoesIniciais = {"Estoque", "Medicamentos", "Receitas", "Sair"};
            int opcaoInicialEscolhida = JOptionPane.showOptionDialog(null, "Bem Vinde(o/a)"
                , "Farmácia", JOptionPane.DEFAULT_OPTION, JOptionPane.INFORMATION_MESSAGE
                , null, opcoesIniciais, opcoesIniciais[0]);

            if (opcaoInicialEscolhida == 0) {
                String[] opcoesEstoque = {"Listar Estoque", "Voltar"};
                int opcaoEstoqueEscolhida = JOptionPane.showOptionDialog(null, "Estoque"
                        , "Farmácia - Estoque", JOptionPane.DEFAULT_OPTION, JOptionPane.INFORMATION_MESSAGE
                        , null, opcoesEstoque, opcoesEstoque[0]);

                if (opcaoEstoqueEscolhida == 0) {
                    try {
                        int escolhaEstoque = Integer.parseInt((String) JOptionPane.showInputDialog(
                                null, mostrarEstoques(estoqueDAO.selecionarEstoques())
                                , "Selecione um Estoque", JOptionPane.PLAIN_MESSAGE
                                , null, null, ""));

                        var medicamentosEstoque = estoqueDAO
                                .selecionarMedicamentosEstoque(escolhaEstoque);

                        JOptionPane.showMessageDialog(null, "Estoque: "
                                + medicamentosEstoque.getNumeroEstoque() + "\nMedicamentos: "
                                + mostrarMedicamentos(medicamentosEstoque.getMedicamentos()));
                    } catch (Exception e) {
                        System.err.println(e.getLocalizedMessage());
                    }
                }

                if (opcaoEstoqueEscolhida == 1) {
                    continue;
                }
            }

            if (opcaoInicialEscolhida == 1) {
                String[] opcoesMedicamentos = {
                        "Selecionar Medicamento", "Checkar Frequência"
                        , "Inserir", "Excluir", "Voltar"
                };

                int opcaoMedicamentoEscolhida = JOptionPane.showOptionDialog(null, "Medicamentos"
                        , "Farmácia - Medicamentos", JOptionPane.DEFAULT_OPTION
                        , JOptionPane.INFORMATION_MESSAGE, null, opcoesMedicamentos, opcoesMedicamentos[0]);

                if (opcaoMedicamentoEscolhida == 0) {
                    try {
                        Medicamento medicamento = medicamentoDAO.selecionarMedicamentoPeloID(
                                Long.parseLong(JOptionPane.showInputDialog(null
                                        , "Digite o código do medicamento: \n" + gerarTextoMedicamentos()))
                        );

                        JOptionPane.showMessageDialog(null, "" + medicamento.getId()
                                + ": " + medicamento.getNome() + "\n" + "   " + medicamento.getDescricao()
                                + "\nPreço: " + medicamento.getPreco()
                                + "\nValidade: " + medicamento.getValidade());
                    } catch (Exception e) {
                        System.err.println(e.getLocalizedMessage());
                    }
                }

                if (opcaoMedicamentoEscolhida == 1) {
                    try {
                        long medicamentoID = Long.parseLong(JOptionPane.showInputDialog(null
                                , gerarTextoMedicamentos()));

                        Medicamento medicamento = medicamentoDAO.selecionarVendasMedicamentos(medicamentoID);
                        JOptionPane.showMessageDialog(null, medicamento.getId() + ": "
                                + "\nMedicamento: " + medicamento.getNome()
                                + "\nVendas: \n" + mostrarVendas(medicamento.getVendasMedicamento()));
                    } catch (Exception e) {
                        System.err.println(e.getLocalizedMessage());
                    }
                }

                if (opcaoMedicamentoEscolhida == 2) {
                    try{
                        Medicamento medicamento = new Medicamento();

                        medicamento.setNome(JOptionPane.showInputDialog("Produto:"));
                        medicamento.setDescricao(JOptionPane.showInputDialog("Descrição(opcional):"));
                        medicamento.setPreco(Double.parseDouble(JOptionPane.showInputDialog("Preço: ")));
                        medicamento.setValidade(translator.toDate(JOptionPane.showInputDialog("Validade: ")));

                        medicamentoDAO.inserirMedicamento(medicamento);
                    } catch(Exception e) {
                        JOptionPane.showMessageDialog(null, e.getMessage());
                    }
                }

                if (opcaoMedicamentoEscolhida == 3) {
                    try {
                        int medicamentoEscolhido =
                                Integer.parseInt(JOptionPane.showInputDialog("Código do Medicamento:"));

                        medicamentoDAO.deletarMedicamento(medicamentoEscolhido);
                    } catch (Exception e) {
                        System.err.println(e.getLocalizedMessage());
                    }
                }

                if (opcaoMedicamentoEscolhida == 4) {
                    continue;
                }
            }

            if (opcaoInicialEscolhida == 2) {
                String[] opcoesReceita = {"Listar Receitas", "Voltar"};
                int opcaoReceitaEscolhida = JOptionPane.showOptionDialog(null, "Receitas"
                        , "Farmácia - Receita", JOptionPane.DEFAULT_OPTION, JOptionPane.INFORMATION_MESSAGE
                        , null, opcoesReceita, opcoesReceita[0]);

                if (opcaoReceitaEscolhida == 0) {
                    try {
                        int escolhaReceita = Integer.parseInt((String) JOptionPane.showInputDialog(
                                null, mostrarReceitas(receitaDAO.selecionarReceitas())
                                , "Selecione a Receita", JOptionPane.PLAIN_MESSAGE
                                , null, null, ""));

                        var medicamentosReceita = receitaDAO.selecionarReceitaMedicamentos(escolhaReceita);

                        JOptionPane.showMessageDialog(null, "Receita: \n"
                                + "Médico: " + medicamentosReceita.getMedicoResponsavel() + "\nCliente: "
                                + medicamentosReceita.getCliente()
                                + "\nMedicamentos: " + mostrarMedicamentos(medicamentosReceita.getMedicametos()));
                    } catch (Exception e) {
                        System.err.println(e.getLocalizedMessage());
                    }
                }

                if (opcaoReceitaEscolhida == 1) {
                    continue;
                }
            }

            if (opcaoInicialEscolhida == 3) {
                break;
            }
        }

        conexao.fecharConexao();
    }

    public String mostrarEstoques(ArrayList<Estoque> listaEstoque) {
        StringBuilder texto = new StringBuilder();

        for (Estoque estoque : listaEstoque) {
            texto
                    .append(estoque.getId())
                    .append(": ")
                    .append("Número de Série: ")
                    .append(estoque.getNumeroEstoque())
                    .append("\n");
        }

        return texto.toString();
    }

    public String mostrarMedicamentos(ArrayList<Medicamento> medicamentos) {
        StringBuilder texto = new StringBuilder();

        for (Medicamento medicamento : medicamentos) {
            texto.append("\n    - ").append(medicamento.getNome());
        }

        return texto.toString();
    }

    public String mostrarReceitas(ArrayList<Receita> listaReceitas) {
        StringBuilder texto = new StringBuilder();

        for (Receita receita : listaReceitas) {
            texto
                    .append(receita.getId()).append(": ")
                    .append("Cliente: ")
                    .append(receita.getCliente())
                    .append("\n");
        }

        return texto.toString();
    }

    public String mostrarVendas(ArrayList<Venda> listaVendas) {
        StringBuilder texto = new StringBuilder();

        for (Venda venda : listaVendas) {
            texto
                    .append("Data: ")
                    .append(venda.getData_venda())
                    .append(", Total: ")
                    .append(venda.getValor_total())
                    .append("\n");
        }

        return texto.toString();
    }

    public String gerarTextoMedicamentos() {
        StringBuilder textoMedicamentos = new StringBuilder();
        MedicamentoDAO dao = new MedicamentoDAO();
        ArrayList<Medicamento> medicamentos = dao.selecionarMedicamentos();
        System.out.println(medicamentos);

        for (Medicamento med : medicamentos) {
            textoMedicamentos.append(med.getId()).append(": ").append(med.getNome()).append("\n");
        }

        return textoMedicamentos.toString();
    }
}
