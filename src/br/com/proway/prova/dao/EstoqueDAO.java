package br.com.proway.prova.dao;

import br.com.proway.prova.core.Conexao;
import br.com.proway.prova.models.Estoque;
import br.com.proway.prova.models.Medicamento;

import javax.swing.*;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class EstoqueDAO {
    public Estoque selecionarMedicamentosEstoque(long estoqueID) {
        Estoque estoque = new Estoque();
        ArrayList<Medicamento> medicamentosEstoque = new ArrayList<>();
        String estoqueQuery = "SELECT * FROM public.estoque e WHERE e.id = " + estoqueID;
        String templateQuery = """
                select
                	e.id, e.numero_estoque,
                	m.id, m.nome, m.descricao, m.preco, m.validade
                from public.estoque e
                	left join public.medicamentos m
                		on m.fk_id_estoque = e.id
                		and e.id =\040""" + estoqueID + """
                where m.id is not null;
                """;

        try {
            Statement statement = Conexao.conexao.createStatement();
            ResultSet resultSet = statement.executeQuery(estoqueQuery);

            if (resultSet.next()) {
                estoque.setId(resultSet.getLong("id"));
                estoque.setNumeroEstoque(resultSet.getString("numero_estoque"));
            }

            statement.close();
            resultSet.close();
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage(), "Banco de Dados"
                    , JOptionPane.INFORMATION_MESSAGE);
        }

        try {
            Statement statement = Conexao.conexao.createStatement();
            ResultSet resultSet = statement.executeQuery(templateQuery);

            while (resultSet.next()) {
                Medicamento medicamento = new Medicamento();
                medicamento.setId(resultSet.getLong("id"));
                medicamento.setNome(resultSet.getString("nome"));
                medicamento.setDescricao(resultSet.getString("descricao"));
                medicamento.setPreco(resultSet.getDouble("preco"));
                medicamento.setValidade(resultSet.getDate("validade"));

                medicamentosEstoque.add(medicamento);
            }

            statement.close();
            resultSet.close();
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage(), "Banco de Dados"
                    , JOptionPane.INFORMATION_MESSAGE);
        }

        estoque.setMedicamentos(medicamentosEstoque);
        return estoque;
    }

    public ArrayList<Estoque> selecionarEstoques() {
        ArrayList<Estoque> listaEstoques = new ArrayList<>();
        String query = "SELECT * FROM public.estoque";

        try {
            Statement statement = Conexao.conexao.createStatement();
            ResultSet resultSet = statement.executeQuery(query);

            while (resultSet.next()) {
                Estoque estoque = new Estoque();

                estoque.setId(resultSet.getLong("id"));
                estoque.setNumeroEstoque(resultSet.getString("numero_estoque"));

                listaEstoques.add(estoque);
            }

            statement.close();
            resultSet.close();
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage(), "Banco de Dados"
                    , JOptionPane.INFORMATION_MESSAGE);
        }

        return listaEstoques;
    }
}
