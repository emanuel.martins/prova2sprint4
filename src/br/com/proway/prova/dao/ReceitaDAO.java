package br.com.proway.prova.dao;

import br.com.proway.prova.core.Conexao;
import br.com.proway.prova.models.Receita;
import br.com.proway.prova.models.Medicamento;

import javax.swing.*;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class ReceitaDAO {
    public Receita selecionarReceitaMedicamentos(long receitaID) {
        Receita receita = new Receita();
        ArrayList<Medicamento> medicamentosReceita = new ArrayList<>();
        String receitaQuery = "SELECT * FROM public.receitas r WHERE r.id = " + receitaID;
        String templateQuery = """
                select
                	r.id, r.medico_responsavel, r.cliente,
                	m.id, m.nome, m.descricao, m.preco, m.validade
                from public.receitas r
                	inner join public.medicamentos_receitas mr
                		on mr.fk_id_receita = r.id
                		and r.id =\040""" + receitaID + """
                	inner join public.medicamentos m
                		on mr.fk_id_medicamento = m.id;
                """;

        try {
            Statement statement = Conexao.conexao.createStatement();
            ResultSet resultSet = statement.executeQuery(receitaQuery);

            if (resultSet.next()) {
                receita.setId(resultSet.getLong("id"));
                receita.setMedicoResponsavel(resultSet.getString("medico_responsavel"));
                receita.setCliente(resultSet.getString("cliente"));
            }

            statement.close();
            resultSet.close();
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage(), "Banco de Dados"
                    , JOptionPane.INFORMATION_MESSAGE);
        }

        try {
            Statement statement = Conexao.conexao.createStatement();
            ResultSet resultSet = statement.executeQuery(templateQuery);

            while (resultSet.next()) {
                Medicamento medicamento = new Medicamento();
                medicamento.setId(resultSet.getLong("id"));
                medicamento.setNome(resultSet.getString("nome"));
                medicamento.setDescricao(resultSet.getString("descricao"));
                medicamento.setPreco(resultSet.getDouble("preco"));
                medicamento.setValidade(resultSet.getDate("validade"));

                medicamentosReceita.add(medicamento);
            }

            statement.close();
            resultSet.close();
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage(), "Banco de Dados"
                    , JOptionPane.INFORMATION_MESSAGE);
        }

        receita.setMedicametos(medicamentosReceita);
        return receita;
    }

    public ArrayList<Receita> selecionarReceitas() {
        ArrayList<Receita> listaReceitas = new ArrayList<>();
        String query = "SELECT * FROM public.receitas";

        try {
            Statement statement = Conexao.conexao.createStatement();
            ResultSet resultSet = statement.executeQuery(query);

            while (resultSet.next()) {
                Receita receita = new Receita();

                receita.setId(resultSet.getLong("id"));
                receita.setCliente(resultSet.getString("cliente"));
                receita.setMedicoResponsavel(resultSet.getString("medico_responsavel"));

                listaReceitas.add(receita);
            }

            statement.close();
            resultSet.close();
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage(), "Banco de Dados"
                    , JOptionPane.INFORMATION_MESSAGE);
        }

        return listaReceitas;
    }
}
