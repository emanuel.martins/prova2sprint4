package br.com.proway.prova.dao;

import br.com.proway.prova.core.Conexao;
import br.com.proway.prova.models.Medicamento;
import br.com.proway.prova.models.Venda;

import javax.swing.*;
import java.sql.*;
import java.util.ArrayList;

public class MedicamentoDAO {
    public ArrayList<Medicamento> selecionarMedicamentos() {
        ArrayList<Medicamento> listaMedicamentos = new ArrayList<>();
        String query = "SELECT m.* FROM public.medicamentos m;";

        try {
            Statement statement = Conexao.conexao.createStatement();
            ResultSet resultSet = statement.executeQuery(query);

            while (resultSet.next()) {
                Medicamento medicamento = new Medicamento();
                medicamento.setId(resultSet.getLong("id"));
                medicamento.setNome(resultSet.getString("nome"));
                medicamento.setDescricao(resultSet.getString("descricao"));
                medicamento.setPreco(resultSet.getDouble("preco"));
                medicamento.setValidade(resultSet.getDate("validade"));

                listaMedicamentos.add(medicamento);
            }

            statement.close();
            resultSet.close();
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage(), "Banco de Dados"
                    , JOptionPane.INFORMATION_MESSAGE);
        }

        return listaMedicamentos;
    }

    public Medicamento selecionarMedicamentoPeloID(long medicamentoID) {
        Medicamento medicamento = new Medicamento();
        String query = "SELECT m.* FROM public.medicamentos m WHERE m.id = "
                + medicamentoID;

        try {
            Statement statement = Conexao.conexao.createStatement();
            ResultSet resultSet = statement.executeQuery(query);

            while (resultSet.next()) {
                medicamento.setId(resultSet.getLong("id"));
                medicamento.setNome(resultSet.getString("nome"));
                medicamento.setDescricao(resultSet.getString("descricao"));
                medicamento.setPreco(resultSet.getDouble("preco"));
                medicamento.setValidade(resultSet.getDate("validade"));
            }

            statement.close();
            resultSet.close();
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage(), "Banco de Dados"
                    , JOptionPane.INFORMATION_MESSAGE);
        }

        return medicamento;
    }

    public Medicamento selecionarVendasMedicamentos(long medicamentoID) {
        Medicamento medicamento = new Medicamento();
        ArrayList<Venda> frequenciaVendas = new ArrayList<>();
        String medicamentosQuery = "SELECT * FROM public.medicamentos m WHERE m.id = "
                + medicamentoID;
        String templateQuery = """
                select
                	m.id, m.nome, m.descricao, m.preco, m.validade,
                	v.id, v.valor_total, v.data_venda
                from public.medicamentos m
                	inner join public.medicamentos_vendas mv
                		on mv.fk_id_medicamento = m.id
                		and m.id =\040""" + medicamentoID + """
                	inner join public.vendas v
                		on mv.fk_id_venda = v.id;
                """;

        try {
            Statement statement = Conexao.conexao.createStatement();
            ResultSet resultSet = statement.executeQuery(medicamentosQuery);

            if (resultSet.next()) {
                medicamento.setId(resultSet.getLong("id"));
                medicamento.setNome(resultSet.getString("nome"));
                medicamento.setDescricao(resultSet.getString("descricao"));
                medicamento.setPreco(resultSet.getDouble("preco"));
                medicamento.setValidade(resultSet.getDate("validade"));
            }

            statement.close();
            resultSet.close();
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage(), "Banco de Dados"
                    , JOptionPane.INFORMATION_MESSAGE);
        }

        try {
            Statement statement = Conexao.conexao.createStatement();
            ResultSet resultSet = statement.executeQuery(templateQuery);

            while (resultSet.next()) {
                Venda venda = new Venda();
                venda.setId(resultSet.getLong("id"));
                venda.setValor_total(resultSet.getDouble("valor_total"));
                venda.setData_venda(resultSet.getDate("data_venda"));

                frequenciaVendas.add(venda);
            }

            statement.close();
            resultSet.close();
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage(), "Banco de Dados"
                    , JOptionPane.INFORMATION_MESSAGE);
        }

        medicamento.setVendasMedicamento(frequenciaVendas);
        return medicamento;
    }

    public void inserirMedicamento(Medicamento medicamento) {
        String query = "INSERT INTO public.medicamentos m ("
                + "nome, descricao, preco, validade) "
                + "VALUES (?, ?, ?, ?)";
        int linhasAtingidas;

        try {
            PreparedStatement statement = Conexao.conexao.prepareStatement(query
                , Statement.RETURN_GENERATED_KEYS);

            statement.setString(1, medicamento.getNome());
            statement.setString(2, medicamento.getDescricao());
            statement.setDouble(3, medicamento.getPreco());
            statement.setDate(4, (Date) medicamento.getValidade());

            linhasAtingidas = statement.executeUpdate();
            if (linhasAtingidas > 0) {
                try {
                    ResultSet resultado = statement.getGeneratedKeys();
                    if (resultado.next()) {
                        medicamento.setId(resultado.getLong(1));
                    }
                } catch (SQLException e) {
                    JOptionPane.showMessageDialog(null, e.getMessage(), "Banco de Dados"
                            , JOptionPane.INFORMATION_MESSAGE);
                }
            }
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage(), "Banco de Dados"
                    , JOptionPane.INFORMATION_MESSAGE);
        }

    }

    public void deletarMedicamento(long id) {
        String query = "DELETE FROM public.medicamentos WHERE id = ?";
        int affectedrows = 0;

        try {
            PreparedStatement statement = Conexao.conexao.prepareStatement(query);
            statement.setLong(1, id);

            affectedrows = statement.executeUpdate();
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage()
                    , "Banco de Dados", JOptionPane.INFORMATION_MESSAGE);
        }
    }
}
