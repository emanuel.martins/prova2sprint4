## 💻 Prova Proway Sprint 4 Parte 2

### Tema: Farmácia 🏥

📕 Docs<br>
Estoque
 - Listagem de todos os medicamentos em um estoque

Receita
 - Listagem de medicamentos em receita médica

Medicamento
 - Selecionar medicamento por ID para exibir detalhes
 - Listar frequência de vendas de um determinado medicamento
 - Adicionar medicamento
 - Excluir medicamento
